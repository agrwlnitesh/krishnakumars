<?php
require_once (__DIR__ . '\..\dbconnect\dbconnect.php');
require_once (__DIR__ . '\loginHistory.php');

//$userName = $_POST['NAME'];
//$sessionID = $_SESSION['SESSIONID'];
class UserData  {
	public static function getUserData($db, $userName=null){
		$db = new dbconnect();
		    
	    try{
		  $db->startTransaction();
		  if($userName == null){
			  $stmt = $db->prepare('SELECT * FROM USER_SESSION_STORE;');
		  }else{
			  $stmt = $db->prepare('SELECT * FROM USER_SESSION_STORE WHERE NAME = :name;');
              $stmt->bindValue(':name', $userName, SQLITE3_TEXT);
		  }
		  
          $result = $stmt->execute();
		 
		 $row = array();
		  $i=0;
		  $flag=true;
		  while($flag){
			  
		  $row[$i] = $result->fetchArray(SQLITE3_ASSOC);
			
			if(!$row[$i]){
				$flag=false;
			}
			$i++;
		  }
		  
		
		  return json_encode($row);
		  //return $result;
		  ////$db->busyTimeout(5000);
		}catch(Exception $exp){
			$db->rollbackTransaction();
		}finally{
			$db->exec('END TRANSACTION;');
			//$db->endTransaction();
			$db->close();
			unset($db);
		}
		return false;
	}
	public static function sessionRegistration($db)
	{
		$userName = $_POST['name'];
		$browser = LoginHistory::getBrowser();
		$ip=LoginHistory::ipAddr();
		$os=LoginHistory::getOS();
		$sessionID=self::createRandomVal(32);
		$hasSession = self::detectSession($db, $userName);
        //$db = new dbconnect();
		    
		try {
			$db->startTransaction();
			if($hasSession==0){
            $query = "INSERT INTO USER_SESSION_STORE VALUES('".$userName."','".$browser."','".$ip."','".$os."','".$sessionID."');";
			$result = $db->exec($query);
			if($result!=false){
				if($isSession=self::setSession($sessionID ))
				 return $isSession;
			    else
					return false;
			}else{
				return false;
			}
			}else{
				$sessionID = self::maintainSession($db, $userName);
				return self::setSession($sessionID);
			}
			////////$db->busyTimeout(5000);
		} catch (Exception $exp) {
			$db->rollbackTransaction();
			return "User Exists";
		} finally {
			//$db->exec('END TRANSACTION;');
			$db->endTransaction();
			//$db->close();
			//unset($db);

		}
	}
	public static function registerUser()
	{
		$userName = $_POST['name'];
		$password = $_POST['password'];
        $db = new dbconnect();
		    
		try {
			$db->startTransaction();
            $query = "INSERT INTO USER(NAME, PASSWORD) VALUES('".$userName."','".$password."');";
			$result = $db->exec($query);
			if($result){
				return true;
			}else{
				return false;
			}
			////////$db->busyTimeout(5000);
		} catch (Exception $exp) {
			$db->rollbackTransaction();
			return "User Exists";
		} finally {
			$db->exec('END TRANSACTION;');
			//$db->endTransaction();
			$db->close();
			unset($db);

		}
	}


	public static function setSession($sessionID){
		session_id($sessionID);
		session_start();
		return true;

	}
    public static function unsetSession($db, $userName){
		
		$stmt = $db->prepare('SELECT SESSIONID FROM USER_SESSION_STORE WHERE NAME = :name');
		$stmt->bindValue(':name',$userName,SQLITE3_TEXT);
		$result = $stmt->execute();
		$row = array();
		  $i=0;
		  $flag=true;
		  while($flag){
			  
		  $row[$i] = $result->fetchArray(SQLITE3_ASSOC);
			
			if(!$row[$i]){
				$flag=false;
			}
			$i++;
		  }
		   return true;
	}
	public static function deleteSession($deleteAll, $userName='',$browser='',$ip=''){
		$db = new dbconnect();
		//$userName =   
		try{
			$db->startTransaction();
		  if($deleteAll=='true'){
			 // $data = json_decode(UserData::getUserData());
			  $query = "DELETE FROM USER_SESSION_STORE WHERE NAME='".$userName."';";
			  //print_r($query);
			  $result = $db->exec($query);
			 //echo $deleteAll.$query;
			 setcookie('PHPSESSID','',time()-42000);
			 
			 return $result;
		  }else{
              $sessionID = self::maintainSession($db, $userName, $browser, $ip);
			  //echo $sessionID."SESSIONID";
			   $query = "DELETE FROM USER_SESSION_STORE WHERE NAME='".$userName."' AND BROWSER='". $browser."' AND IP='".$ip."';";
              /*$stmt = $db->prepare('DELETE FROM USER_SESSION_STORE WHERE NAME=:name AND BROWSER= :browser AND IP=:ip;');
			  $stmt->bindValue(':name', $userName, SQLITE3_TEXT);
		      $stmt->bindValue(':browser', $browser, SQLITE3_TEXT);
		      $stmt->bindValue(':ip', $ip, SQLITE3_TEXT); 
		      $result = $stmt->execute();*/
			  $result = $db->exec($query);
			  if($result!=false)
				 //echo $query;
			     setcookie('PHPSESSID',$sessionID,time()-42000);
				 return true;
		  }
		  
		  
		
		 // return $result;
		}catch(Exception $exp){
			$db->rollbackTransaction();
		}finally{
			$db->exec('END TRANSACTION;');
			//$db->endTransaction();
			$db->close();
			unset($db);
		}
	}
	
	public static function detectSession($db,$userName, $browser=null, $ip=null){
		//$db = new dbconnect();
		  
		try{
			/* if(isset($_COOKIE['PHPSESSID'])){
			  $sessionID = $_COOKIE['PHPSESSID'];
		    }else{
			  $sessionID = '';
			} */
		  //$db->startTransaction();
          $stmt = $db->prepare('SELECT COUNT(*) AS COUNT FROM USER_SESSION_STORE WHERE NAME=:name AND BROWSER= :browser AND IP= :ip AND OS=:os;');
		  $stmt->bindValue(':name', $userName, SQLITE3_TEXT);
		  $stmt->bindValue(':browser', LoginHistory::getBrowser(), SQLITE3_TEXT);
		  $stmt->bindValue(':ip', LoginHistory::ipAddr(), SQLITE3_TEXT);
		  $stmt->bindValue(':os', LoginHistory::getOS(), SQLITE3_TEXT);
		  $result = $stmt->execute();
		  // unset($db);
		  $row = $result->fetchArray(SQLITE3_ASSOC);
		  // print_r(LoginHistory::getClient());	
		  
		  return ($row['COUNT']<>0);
		  //true;//$row[0]['COUNT'];
		  //$db->busyTimeout(2000);
		}catch(Exception $exp){
			$db->rollbackTransaction();
		}finally{
			//$db->exec('END TRANSACTION;');
			//$db->endTransaction();
			//$db->close();
			//unset($db);
		}
	}
	
	public function maintainSession($db, $userName, $browser=null,$ip=null ){
		//$db = new dbconnect();
		    
		try{
			if($browser==null)
				$browser = LoginHistory::getBrowser();
			if($ip==null)
				$ip = LoginHistory::ipAddr();
			
			$stmt = $db->prepare('SELECT SESSIONID FROM USER_SESSION_STORE WHERE NAME = :name AND BROWSER = :browser AND IP = :ip LIMIT 1;' );
		    $stmt->bindValue(':name', $userName, SQLITE3_TEXT);
		    $stmt->bindValue(':browser', $browser, SQLITE3_TEXT);
		    $stmt->bindValue(':ip', $ip, SQLITE3_TEXT);
		    $result = $stmt->execute();
			$row = $result->fetchArray(SQLITE3_ASSOC);
			
		    
		    
		  //}
          
		  return $row['SESSIONID'];
		  ////$db->busyTimeout(5000);
		}catch(Exception $exp){
			$db->rollbackTransaction();
		}finally{
			//$db->exec('END TRANSACTION;');
			//$db->endTransaction();
			//$db->close();
			//unset($db);
		}
	}
	
	public static function createRandomVal($val){
      $chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      srand((double)microtime()*1000000);
      $i = 0;
      $pass = '' ;
      while ($i<=$val) 
    {
        $num  = rand() % 33;
        $tmp  = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
      }
    return $pass;
    }
}
/*//$res = UserData::getUserData('krishna');*/
//echo UserData::registerUserSession('krishna')."this is result";
//echo UserData::detectSession('krishna')."this is result";
/*//$res = UserData::maintainSession('v','::1','a');
$res = UserData::deleteSession('v','::1','a');
//return print_r(UserData::getUserData('krishna'));
if(!$res){
	echo 'selected';
	print_r($res);
	//echo $res;
}else{
	//while($row = $res->fetchArray()){}
	print_r($res);
}*/
//$res = UserData::registerUserSession();
?>