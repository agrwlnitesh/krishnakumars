<?php
/**
 * Created by PhpStorm.
 * User: i309848
 * Date: 8/19/16
 * Time: 12:02 AM
 */
require_once (__DIR__ . '\UserData.php');
$db = new dbconnect();

if(isset($_POST['checkSession'])&& $_POST['checkSession'] == true && isset($_COOKIE['PHPSESSID'])){
    if(UserData::detectSession($db, $_POST['name'], LoginHistory::getBrowser(), LoginHistory::getOS())){
		echo UserData::getUserData($db, $_POST['name']);
		return;
	}
	$db->close();
	return false;
}elseif(isset($_POST['fLogOut'])&& $_POST['fLogOut'] === 'true'){
    echo UserData::deleteSession(true,$_POST['name']);
	return;
}elseif(isset($_POST['fLogOut'])&& $_POST['fLogOut'] === 'false'){
    echo UserData::deleteSession( $_POST['fLogOut'], $_POST['name'], $_POST['browser'],$_POST['ip']);
	return ;
}elseif(isset($_POST['registerEvent'])&& $_POST['registerEvent'] == true){
	echo UserData::registerUser();
	return;
}else{
	echo "No session";
}
?>